import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NoteModel } from '../common/note.model';
import { NoteService } from '../note-service.service';

@Component({
  selector: 'app-note-create',
  templateUrl: './note-create.component.html',
  styleUrls: ['./note-create.component.css']
})
export class NoteCreateComponent implements OnInit {
  editId: number;
  noteMessage = ""
  isEditmode = false;
  editNote: NoteModel;
  noteForm: FormGroup;

  constructor(private noteService: NoteService, private route: ActivatedRoute, private router: Router) { }
  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.editId = params['id']
      this.isEditmode = this.editId != null;
      this.initForm()
    })
  }
  createNewNote(): void {
    if (!this.isEditmode) {
      this.noteMessage = "Note Added SuccesFully !"
      this.noteService.addNewNote(this.noteForm.value)
      this.router.navigate([''])
    } else {
      this.noteMessage = "Note Updated SuccesFully !"
      this.noteService.editNote(this.editId, this.noteForm.value)
      this.router.navigate([''])
    }
  }
  initForm() {
    let title = '';
    let description = '';
    if (this.isEditmode) {
      const note = this.noteService.getNoteByIndex(this.editId)
      title = note.title;
      description = note.description;
    }
    this.noteForm = new FormGroup({
      "title": new FormControl(title, Validators.required),
      "description": new FormControl(description, Validators.required)
    })
  }
}
