import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NoteModel } from '../common/note.model';
import { NoteService } from '../note-service.service';


@Component({
  selector: 'app-note-list',
  templateUrl: './note-list.component.html',
  styleUrls: ['./note-list.component.css']
})
export class NoteListComponent implements OnInit {
  notes: NoteModel[] = [];
  task: string = 'new';
  itemId: number;
  constructor(private noteService: NoteService, private route: Router) { }
  ngOnInit(): void {
    this.notes = this.noteService.getNotes()
    console.log(this.notes);
  }
  onDelete(index: number): void {
    this.noteService.deleteNote(index)
    this.task = 'delete'
    this.changeID(index)
  }
  onNewNote() {
    this.task = 'new'
    this.route.navigate(['newnote'])
  }
  onEditNote(index: number): void {
    this.route.navigate(['editnote', index])
    this.task = 'edit'
    this.changeID(index)
  }
  onNoteClick(index: number): void {
    this.route.navigate(['viewnote', index],)
    this.changeID(index)
  }
  changeID(index: number) {
    this.itemId = index
    console.log(this.itemId);

  }

}
