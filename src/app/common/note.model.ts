export interface NoteModel {
    title: string;
    description: string;
    created: Date;
    deleted?: boolean;
    updated?: boolean;
    newNote?: boolean;
}