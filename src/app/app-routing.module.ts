import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { NoteCreateComponent } from './note-create/note-create.component';
import { NoteDetailsComponent } from './note-details/note-details.component';
import { NoteListComponent } from './note-list/note-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'notes', pathMatch: 'full' },
  {
    path: 'viewnote', component: NoteDetailsComponent, children: [
      { path: ':id', component: NoteDetailsComponent }
    ]
  },
  { path: 'notes', component: NoteListComponent },
  { path: 'newnote', component: NoteCreateComponent },
  { path: 'editnote/:id', component: NoteCreateComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
