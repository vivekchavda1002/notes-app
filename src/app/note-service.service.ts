import { Injectable } from '@angular/core';
import { NoteModel } from './common/note.model';


@Injectable({
  providedIn: 'root'
})
export class NoteService {
  noteList: NoteModel[] = [
    { title: 'Essentials', description: "Bring 3/kg Lemon From Market.", created: new Date() },
    { title: 'Pending', description: "Upload Third Practical of Angular Before EOD", created: new Date() },
    { title: 'Spare Time', description: "Go to Gym at 5 PM via Bicycle", created: new Date() },
    { title: 'Important', description: "Calculate Total Expense of this month", created: new Date() },
    { title: 'Reminder', description: "Dont Forgot to Recharge Dads Phone.", created: new Date() }
  ];
  constructor() { }
  getNotes() {
    return this.noteList;
  }
  addNewNote(note: NoteModel) {
    const newNote = {
      ...note,
      created: new Date(),
      newNote: true
    }
    this.noteList.push(newNote);
  }

  deleteNote(index: number) {
    let deleteNote = this.noteList[index];
    deleteNote = {
      ...deleteNote,
      deleted: true
    }
    this.noteList[index] = deleteNote;
  }
  getNoteByIndex(index: number) {
    return this.noteList.slice()[index]
  }
  editNote(index: number, note: NoteModel) {
    let updateNote = this.noteList[index];
    updateNote = {
      ...note,
      updated: true
    }
    this.noteList[index] = updateNote;
  }

}
