import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NoteModel } from '../common/note.model';
import { NoteService } from '../note-service.service';

@Component({
  selector: 'app-note-details',
  templateUrl: './note-details.component.html',
  styleUrls: ['./note-details.component.css']
})
export class NoteDetailsComponent implements OnInit {

  singleNote: NoteModel;
  id;
  constructor(private route: ActivatedRoute, private noteService: NoteService) { }

  ngOnInit(): void {
    this.id = this.route.firstChild.params['value'].id;
    this.singleNote = this.noteService.getNoteByIndex(this.id)
  }
}
